import ApiRequest from './ApiRequest';

/**
 * @param {String} endPoint - API endpoint url
 * @param {?String} method - Request method
 * @param {?Object} params - Request params
 * @return {Promise}
 */
// const templateFn = (endPoint, method, params = {}, options = {}) => new ApiRequest({ url: endPoint, method, data: params, ...options, cancelToken: source.token }).load();
const templateFn = (endPoint, method, params = {}, options = {}) =>
  new ApiRequest({ url: endPoint, method, data: params, ...options }).load();

export default class Api {
  /* ----- Login ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static login(params) {
    return templateFn('/login', ApiRequest.METHOD_POST, params);
  }
  /**
   * @return {Promise}
   */
  static logout() {
    return templateFn('/logout', ApiRequest.METHOD_POST, {}).catch(() => ({
      status: 401
    }));
  }
  /* ----- Forgot ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static forgot(params) {
    return templateFn('/forgot', ApiRequest.METHOD_POST, params);
  }
  /* ----- Reset ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static reset(params) {
    return templateFn(`/reset`, ApiRequest.METHOD_POST, JSON.stringify(params));
  }

  /* ----- User ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static getUser(params) {
    return templateFn('/me', ApiRequest.METHOD_GET, JSON.stringify(params));
    // .then(() => true)
    // .catch(() => false);
  }
  /* ----- User Profile ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static getProfile(params) {
    return templateFn(
      '/user-profile',
      ApiRequest.METHOD_POST,
      JSON.stringify(params)
    );
  }

  /* ----- User Update Profile ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static updateProfile(id, params) {
    return templateFn(
      `/edit/${id}`,
      ApiRequest.METHOD_POST,
      JSON.stringify(params)
    );
  }

  /* ----- User Register ----- */
  /**
   * @param {Object} params
   * @return {Promise}
   */
  static register(params) {
    return templateFn('/register', ApiRequest.METHOD_POST, params);
  }
}
