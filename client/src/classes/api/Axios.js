import axios from 'axios';

// const BASE_URI = 'http://127.0.0.1:7000/api/auth';

export default axios.create({
  baseURL: process.env.VUE_APP_BASE_URI
});
