import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { VueReCaptcha } from 'vue-recaptcha-v3';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

library.add(fas, fab, far);

const app = createApp(App);
import axios from './classes/api/Axios';
app.config.globalProperties.$http = axios;
app
  .component('icon', FontAwesomeIcon)
  .use(router)
  .use(store)
  .use(VueReCaptcha, { siteKey: process.env.VUE_APP_GOOGLERECAPTCHAKEYPROD })
  .mount('#app');
