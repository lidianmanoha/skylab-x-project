import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home';
import Login from '@/components/molecules/Login';
import Register from '@/components/molecules/Register';
import Forgot from '@/components/molecules/Forgot';
import Reset from '@/components/molecules/Reset';
import NotFound from '@/components/molecules/NotFound';
import UserAccount from '@/components/molecules/UserAccount';
import store from '../store';
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/forgot',
    name: 'forgot',
    component: Forgot
  },
  {
    path: '/reset/:token',
    name: 'reset',
    component: Reset
  },
  {
    path: '/edit/:id',
    name: 'userAccount',
    component: UserAccount,
    beforeEnter: (to, from, next) => {
      let user = store.getters['user/user'];
      if (to.name !== 'login' && !user) next({ name: 'login' });
      else next();
    }
  },
  {
    path: '/:catchAll(.*)',
    component: NotFound,
    name: '404'
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
