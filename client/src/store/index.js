import { createStore } from 'vuex';
import userModules from './modules/User';
export default createStore({
  modules: {
    user: userModules
  },
  state: {},
  getters: {},
  actions: {},
  mutations: {}
});
