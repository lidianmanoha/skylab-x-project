export default {
  user(context, user) {
    context.commit('user', user);
  }
};
