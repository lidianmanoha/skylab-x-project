const CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  devServer: {
    host: 'localhost'
  },
  configureWebpack: {
    output: {
      crossOriginLoading: 'anonymous'
    },
    plugins: [
      new CompressionPlugin({
        filename: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(js|css|png|jpg|jpeg|gif|ico|json|woff|ttf|otf|svg|woff2|eot)$/,
        threshold: 10240,
        minRatio: 0.8
      })
    ]
  },

  chainWebpack: config => {
    /* 
       Disable (or customize) prefetch, see:
       https://cli.vuejs.org/guide/html-and-static-assets.html#prefetch
    */
    config.plugins.delete('prefetch');

    /* 
       Configure preload to load all chunks
       NOTE: use `allChunks` instead of `all` (deprecated)
    */
    config.plugin('preload').tap(options => {
      options[0].include = 'allChunks';
      return options;
    });
    config.plugin('CompressionPlugin').use(CompressionPlugin);
  }
};
