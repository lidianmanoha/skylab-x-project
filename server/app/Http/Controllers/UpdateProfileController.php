<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use GuzzleHttp\Psr7\Message;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UpdateProfileController extends Controller
{
    /**
     * Update information a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    function showUser($id)
    {

        $user = User::find($id);
        return Response()->json($user);
    }


    public function userProfileUpdate(UpdateProfileRequest $request, $id)
    {
        $user = User::find($id);


        if (User::where('id', $id)->doesntExist()) {
            return response([
                'message' => "User doen't exists!"
            ], 404);
        }
        if ($user !== $id) {
            $userUpdate = [
                'id' => $request->id,
                'firstname' =>  $request->firstname,
                'lastname' =>  $request->lastname,
                'email' =>  $request->email,
                'password' =>  $request->password = Hash::make($request->input('password')),

            ];
            DB::table('users')->where('id', $id)->update($userUpdate);
            return response()->json([
                'message' => 'User successfully updated',
                'user' => $userUpdate,
            ], 201);
        } else {

            return response(['message' => 'error Update']);
        }
    }
}
