<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory()->count(5)->create();
        DB::table('users')->insert(

            array(
                array(
                    'firstname' => 'jane',
                    'lastname' => 'doe',
                    'email' => 'jane@yopmail.com',
                    'password' => bcrypt('123456'),
                ),
                array(
                    'firstname' => 'john',
                    'lastname' => 'doe',
                    'email' => 'john@yopmail.com',
                    'password' => bcrypt('123456'),
                ),
            )

        );
    }
}
