<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::get('user', 'App\Http\Controllers\AuthController@user');
    Route::post('register', 'App\Http\Controllers\AuthController@register');
    Route::post('forgot', 'App\Http\Controllers\ForgotController@forgot');
    Route::post('reset', 'App\Http\Controllers\ForgotController@reset');
    Route::post('user-profile', 'App\Http\Controllers\AuthController@userProfile');
    Route::post('edit/{id}', 'App\Http\Controllers\UpdateProfileController@userProfileUpdate');
    Route::get('editUser/{id}', 'App\Http\Controllers\UpdateProfileController@showUser');
});

Route::any('{any}', function () {
    return response()->json([
        'status' => 'error',
        'message' => 'Resource not found'
    ], 404);
})->where('any', '.*');
// Auth::routes();
